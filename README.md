# Script to fix broken QTI tests in OLAT

## Requirements

* Python 3

## Install

```bash

  git clone https://gitlab.com/olatsystems/fix_qti1_tests.git
  cd fix_qti1_tests
  python3 -m venv .
  bin/pip install -r requirements.txt

```

## Run

```bash

  bin/python3 fix_qti1_tests.py <QTI_TEST_AS_ZIP_EXPORT>
  
```

There is an example included

```bash

  bin/python3 fix_qti1_tests.py test/qti_NO.zip

```

This will create a `qti_NO_FIXED.zip` in the `test` folder. You need to import this file as a learning resource
an replace the broken test in your course with it.


## Troubleshooting

1. You need an export of the test as learning resource. The `qti.xml` needs to be top level.
   An "linked" export together with a course will not work. 

2. The script is not idempotent right now. It does not check, if a test is broken or not.
   It only adds a XML declaration to the `qti.xml` file. If it is already there it is doubled,
   which will give other problems most likely.
